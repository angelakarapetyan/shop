package am.energizeglobalservices.shop.security;

import am.energizeglobalservices.shop.persistence.entity.UserEntity;
import am.energizeglobalservices.shop.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class CurrentUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserEntity user = userRepository.findByEmail(s);
        if (user == null) {
            throw new UsernameNotFoundException("user by email " + s + " not found");
        }
        return new CurrentUser(user);
    }
}
