package am.energizeglobalservices.shop.security;

import am.energizeglobalservices.shop.persistence.entity.UserEntity;
import org.springframework.security.core.authority.AuthorityUtils;

public class CurrentUser extends org.springframework.security.core.userdetails.User {

    private UserEntity user;

    public CurrentUser(UserEntity user) {
        super(user.getEmail(), user.getPassword(), AuthorityUtils.createAuthorityList(user.getType()));
        this.user=user;
    }

    public UserEntity getUser() {
        return user;
    }
}
