package am.energizeglobalservices.shop.service.dto;

import am.energizeglobalservices.shop.persistence.entity.CategoryEntity;
import am.energizeglobalservices.shop.persistence.entity.CommentEntity;
import am.energizeglobalservices.shop.persistence.entity.ProductEntity;
import org.modelmapper.ModelMapper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProductDto implements Serializable {

    private static final ModelMapper modelMapper = new ModelMapper();

    private Long id;
    private String name;
    private Double price;
    private Integer rate;
    private Integer countRate;
    private CategoryEntity category;
    private List<CommentEntity> comments;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Integer getCountRate() {
        return countRate;
    }

    public void setCountRate(Integer countRate) {
        this.countRate = countRate;
    }

    public CategoryEntity getCategory() {
        return category;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }

    public List<CommentEntity> getComments() {
        return comments;
    }

    public void setComments(List<CommentEntity> comments) {
        this.comments = comments;
    }

    public static ProductEntity mapDtoToEntity(ProductDto dto) {

        return modelMapper.map(dto, ProductEntity.class);
    }

    public static List<ProductDto> mapEntitesToDtos(List<ProductEntity> entities) {
        List<ProductDto> dtos = new ArrayList<>();
        entities.forEach(entity -> dtos.add(ProductDto.mapEntityToDto(entity)));

        return dtos;
    }

    public static ProductDto mapEntityToDto(ProductEntity entity) {
        if (entity == null) {
            return null;
        }

        return modelMapper.map(entity, ProductDto.class);
    }
}
