package am.energizeglobalservices.shop.service.dto;

import am.energizeglobalservices.shop.persistence.entity.CommentEntity;
import am.energizeglobalservices.shop.persistence.entity.UserEntity;
import org.modelmapper.ModelMapper;

import java.io.Serializable;
import java.util.List;

public class UserDto implements Serializable {

    private static final ModelMapper modelMapper = new ModelMapper();

    public enum UserType {
        USER,
        ADMIN
    }

    public enum UserStatus {
        ACTIVE,
        BLOCKED
    }

    private Long id;
    private String name;
    private String surname;
    private String email;
    private String username;
    private String password;
    private UserType type;
    private UserStatus status;

    private List<CommentEntity> comments;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<CommentEntity> getComments() {
        return comments;
    }

    public void setComments(List<CommentEntity> comments) {
        this.comments = comments;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public static UserDto mapEntityToDto(UserEntity entity) {

        return modelMapper.map(entity, UserDto.class);
    }

    public static UserEntity mapDtoToEntity(UserDto dto) {

        return modelMapper.map(dto, UserEntity.class);
    }
}
