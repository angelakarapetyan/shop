package am.energizeglobalservices.shop.service;

import am.energizeglobalservices.shop.exception.EntityNotFoundException;
import am.energizeglobalservices.shop.persistence.entity.UserEntity;
import am.energizeglobalservices.shop.persistence.repository.UserRepository;
import am.energizeglobalservices.shop.service.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public UserDto createUser(UserDto dto) {
        UserEntity userEntity = UserDto.mapDtoToEntity(dto);
        userEntity.setPassword(passwordEncoder.encode(dto.getPassword()));
        userEntity = userRepository.save(userEntity);

        return UserDto.mapEntityToDto(userEntity);
    }

    public UserDto updateStatus(Long userId, UserDto.UserStatus status) {
        UserEntity user = userRepository.findById(userId).orElseThrow(
                () -> new EntityNotFoundException("user", userId)
        );

        user.setStatus(status.name());
        user = userRepository.save(user);

        return UserDto.mapEntityToDto(user);
    }
}