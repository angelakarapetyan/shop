package am.energizeglobalservices.shop.service;

import am.energizeglobalservices.shop.persistence.entity.UserEntity;
import am.energizeglobalservices.shop.persistence.repository.UserRepository;
import am.energizeglobalservices.shop.service.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AdminService implements CommandLineRunner {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AdminService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) {
        UserEntity admin = userRepository.findByEmail("admin@mail.ru");
        if (admin == null) {
            admin = new UserEntity();
            admin.setName("admin");
            admin.setUsername("admin");
            admin.setEmail("admin@mail.ru");
            admin.setSurname("admin");
            admin.setType(UserDto.UserType.ADMIN.name());
            admin.setStatus(UserDto.UserStatus.ACTIVE.name());
            admin.setPassword(passwordEncoder.encode("admin"));

            userRepository.save(admin);
        }
    }
}
