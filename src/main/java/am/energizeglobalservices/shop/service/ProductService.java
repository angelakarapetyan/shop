package am.energizeglobalservices.shop.service;

import am.energizeglobalservices.shop.exception.EntityNotFoundException;
import am.energizeglobalservices.shop.persistence.entity.CommentEntity;
import am.energizeglobalservices.shop.persistence.entity.ProductEntity;
import am.energizeglobalservices.shop.persistence.entity.UserEntity;
import am.energizeglobalservices.shop.persistence.repository.CommentRepository;
import am.energizeglobalservices.shop.persistence.repository.ProductRepository;
import am.energizeglobalservices.shop.service.dto.CommentDto;
import am.energizeglobalservices.shop.service.dto.ProductDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final CommentRepository commentRepository;

    @Autowired
    public ProductService(ProductRepository productRepository,
                          CommentRepository commentRepository) {
        this.productRepository = productRepository;
        this.commentRepository = commentRepository;
    }

    public List<ProductDto> getProducts(String name, Integer rate,
                                        Double startPrice, Double endPrice) {


        List<ProductEntity> productEntities = productRepository.findAll(name == null ? "" : name,
                rate, startPrice, endPrice);

        return ProductDto.mapEntitesToDtos(productEntities);
    }

    public ProductDto createProduct(ProductDto dto) {

        ProductEntity productEntity = ProductDto.mapDtoToEntity(dto);
        productEntity = productRepository.save(productEntity);

        return ProductDto.mapEntityToDto(productEntity);
    }

    public ProductDto updateProduct(ProductDto dto) {

        ProductEntity productEntity = ProductDto.mapDtoToEntity(dto);
        productEntity = productRepository.save(productEntity);

        return ProductDto.mapEntityToDto(productEntity);
    }

    public void deleteProduct(Long productId) {
        productRepository.deleteById(productId);
    }

    public ProductDto leaveCommentOnProduct(UserEntity currentUser, CommentDto dto) {
        Long productId = dto.getProduct().getId();
        ProductEntity product = productRepository.findById(productId).orElseThrow(
                () -> new EntityNotFoundException("product", productId)
        );

        CommentEntity comment = new CommentEntity();
        comment.setContent(dto.getContent());
        comment.setCreatedDate(new Date());
        comment.setProduct(product);
        comment.setUser(currentUser);

        commentRepository.save(comment);

        return ProductDto.mapEntityToDto(product);
    }

    public ProductDto rateProduct(Long productId, Integer rate) {
       ProductEntity product =  productRepository.findById(productId).orElseThrow(
                () -> new EntityNotFoundException("product", productId));

        int newRate = calculateRate(product.getRate(), product.getCountRate(), rate);

        product.setRate(newRate);
        product.setCountRate(product.getCountRate() + 1);

        productRepository.save(product);

        return ProductDto.mapEntityToDto(product);
    }

    private int calculateRate( int totalRate, int countRate, int rate) {
        int newRate = (totalRate * countRate + rate) / (countRate + 1);

        return newRate;
}
}
