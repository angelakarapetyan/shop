package am.energizeglobalservices.shop.exception;

public class RequestParamMissingException extends RuntimeException {

    public RequestParamMissingException(String paramName) {
        super(String.format("Param '%s' is required", paramName));
    }
}
