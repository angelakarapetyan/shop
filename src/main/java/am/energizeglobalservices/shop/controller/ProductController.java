package am.energizeglobalservices.shop.controller;

import am.energizeglobalservices.shop.exception.RequestParamMissingException;
import am.energizeglobalservices.shop.persistence.entity.UserEntity;
import am.energizeglobalservices.shop.service.ProductService;
import am.energizeglobalservices.shop.service.dto.CommentDto;
import am.energizeglobalservices.shop.service.dto.ProductDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    @Autowired
    ProductController(ProductService productService) {
        this.productService = productService;
    }


    @GetMapping
    public List<ProductDto> getProducts(@RequestParam(required = false) String name,
                                        @RequestParam(required = false) Integer rate,
                                        @RequestParam(required = false) Double startPrice,
                                        @RequestParam(required = false) Double endPrice) {

        return productService.getProducts(name, rate, startPrice, endPrice);
    }

    @PostMapping
    public ProductDto createProduct(@RequestBody ProductDto dto) {
        if (dto.getName() == null || dto.getName().trim().equals("")) {
            throw new RequestParamMissingException("name");
        }

        if (dto.getCategory() == null) {
            throw new RequestParamMissingException("category");
        }

        if (dto.getPrice() == null) {
            throw new RequestParamMissingException("price");
        }

        return productService.createProduct(dto);
    }

    @PutMapping
    public ProductDto updateProduct(@RequestBody ProductDto dto) {

        return productService.updateProduct(dto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteProduct(@PathVariable Long id) {
        productService.deleteProduct(id);

        return ResponseEntity.ok().build();
    }

    @PostMapping("/{id}/comments")
    public ProductDto leaveCommentOnProduct(@RequestBody CommentDto dto,
                                            @AuthenticationPrincipal UserEntity currentUser) {
        if (dto.getContent() == null || dto.getContent().trim().equals("")) {
            throw new RequestParamMissingException("content");
        }
        if (dto.getProduct().getId() == null) {
            throw new RequestParamMissingException("productId");
        }

        return productService.leaveCommentOnProduct(currentUser, dto);
    }

    @PatchMapping("/{id}/rate")
    public ProductDto rateProduct(@PathVariable Long id, @RequestParam("rate") Integer rate) {
        return productService.rateProduct(id, rate);
    }
}
