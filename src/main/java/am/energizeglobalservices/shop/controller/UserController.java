package am.energizeglobalservices.shop.controller;


import am.energizeglobalservices.shop.exception.RequestParamMissingException;
import am.energizeglobalservices.shop.security.CurrentUserDetailsService;
import am.energizeglobalservices.shop.security.jwt.JwtAuthenticationRequest;
import am.energizeglobalservices.shop.security.jwt.JwtTokenUtil;
import am.energizeglobalservices.shop.service.UserService;
import am.energizeglobalservices.shop.service.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final CurrentUserDetailsService userDetailsService;

    @Autowired
    public UserController(UserService userService,
                          AuthenticationManager authenticationManager,
                          JwtTokenUtil jwtTokenUtil,
                          CurrentUserDetailsService userDetailsService) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userDetailsService = userDetailsService;
    }

    @PostMapping(value = "/sign-in")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest) throws AuthenticationException {

        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticationRequest.getEmail(),
                        authenticationRequest.getPassword()
                )
        );
        UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getEmail());
        final String token = jwtTokenUtil.generateToken(userDetails.getUsername());

        return ResponseEntity.ok(token);
    }

    @PostMapping(value = "/sign-up")
    public UserDto createUser(@RequestBody UserDto dto) {

        if (dto.getName() == null || dto.getName().trim().equals("")) {
            throw new RequestParamMissingException("name");
        }
        if (dto.getSurname() == null || dto.getSurname().trim().equals("")) {
            throw new RequestParamMissingException("surname");
        }
        if (dto.getUsername() == null || dto.getUsername().trim().equals("")) {
            throw new RequestParamMissingException("username");
        }
        if (dto.getEmail() == null || dto.getEmail().trim().equals("")) {
            throw new RequestParamMissingException("email");
        }
        if (dto.getPassword() == null || dto.getPassword().trim().equals("")) {
            throw new RequestParamMissingException("password");
        }

        return userService.createUser(dto);
    }

    @PatchMapping(value = "/{id}/status")
    public UserDto updateUserStatus(@PathVariable Long id, @RequestBody
            Map<String, UserDto.UserStatus> details) {
        UserDto.UserStatus status = details.get("status");
        if (status == null) {
            throw new RequestParamMissingException("status");
        }

        return userService.updateStatus(id, status);
    }
}
