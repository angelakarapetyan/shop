package am.energizeglobalservices.shop.persistence.repository;

import am.energizeglobalservices.shop.persistence.entity.CommentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<CommentEntity, Long> {
}
