package am.energizeglobalservices.shop.persistence.repository;

import am.energizeglobalservices.shop.persistence.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends JpaRepository<ProductEntity, Long> {

    @Query(value = "SELECT p FROM ProductEntity p " +
            "WHERE (:name = '' OR  LOWER(p.name) LIKE ('%' || LOWER(:name) || '%')) " +
            "AND (:startPrice IS NULL OR :endPrice IS NULL OR p.price BETWEEN :startPrice AND :endPrice) " +
            "AND (:rate IS NULL OR p.rate = :rate) " +
            "ORDER BY p.price")
    List<ProductEntity> findAll(@Param("name") String name,
                                @Param("rate") Integer rate,
                                @Param("startPrice") Double startPrice,
                                @Param("endPrice") Double endPrice);

    List<ProductEntity> findAll();
}
