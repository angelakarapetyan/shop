package am.energizeglobalservices.shop.persistence.repository;

import am.energizeglobalservices.shop.persistence.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
    UserEntity findByEmail(String email);
}
